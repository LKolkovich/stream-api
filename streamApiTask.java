import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.function.Function;
import java.util.List;
import java.lang.String;
import java.util.Comparator;

public class streamApiTask {
    public static void main(String[] args) {
        Comparator<Counter> customComparator = new Comparator<Counter>() {
            @Override
            public int compare(Counter o1, Counter o2) {
                if (o1.getCount() == o2.getCount())
                    return o1.getWord().compareTo(o2.getWord());
                else if (o1.getCount() < o2.getCount())
                    return 1;
                else return -1;
            }
        };

        Scanner myScan = new Scanner(System.in, StandardCharsets.UTF_8);
        String str = myScan.nextLine();
        str = str.toLowerCase();
        List<String> words = new ArrayList<String>();
        String word = "";
        for(int i = 0; i < str.length(); i++){
            char sim = str.charAt(i);
            if(Character.isLetter(sim) || Character.isDigit(sim))
                word = word + sim;
            else{
                if(word.length() != 0){
                    words.add(word);
                }
                word = "";
            }
        }
        if(word.length() != 0){
            words.add(word);
        }

        words.stream()
                .map(ToCounter(words))
                .sorted(customComparator)
                .distinct()
                .limit(10)
                .forEach(Counter -> System.out.println(Counter.getWord()));
    };
        static Function<String, Counter> ToCounter(List<String> words){
            return word -> new Counter(word, Collections.frequency(words, word));
        }
}

class Counter{
    public Counter(String word, int count) {
        this.word = word;
        this.count = count;
    }
    public String getWord() {
        return word;
    }
    public int getCount() {
        return count;
    }

    @Override
    public int hashCode() {
        return word.hashCode();
    }
    @Override
    public boolean equals(Object obj){
        Counter other = (Counter) obj;
        if (word.equals(other.word))
            return true;
        return false;
    }

    private String word;
    private int count;
}